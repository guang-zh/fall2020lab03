/**
 * Guang Kun Zhang
 * 1942372
 * Lab3 JavaIII Testing
 */

package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 * @author guang
 * {@link https://gitlab.com/guang-zh/fall2020lab03.git}
 */

public class Vector3dTests {
	
	// Test getValues() methods
	@Test
	public void testGets() {
		Vector3d vec1 = new Vector3d(8, 21, 6);
		assertEquals(8, vec1.getX());
		System.out.println("x: "+ vec1.getX());
		assertEquals(21, vec1.getY());
		System.out.println("y: "+ vec1.getY());
		assertEquals(6, vec1.getZ());
		System.out.println("z: "+ vec1.getZ());
	}
	
	// Test magnitude() method
	@Test
	public void testMagnitude() {
		Vector3d vec1 = new Vector3d(-10, 2.5, 4);
		double expected = Math.sqrt(Math.pow(-10, 2)+2.5*2.5+4*4);
		assertEquals(expected, vec1.magnitude());
		System.out.println("Magnitude() method testing");
	}
	
	// Test dotProduct() method
	@Test
	public void testDotProduct() {
		Vector3d vec1 = new Vector3d(2, 3, 4);
		Vector3d vec2 = new Vector3d(5, 6, 7);
		double expected = 2*5+3*6+4*7;
		assertEquals(expected, vec1.dotProduct(vec2));
		System.out.println("DotProduct return: "+ vec1.dotProduct(vec2));
	}
	
	// Test add() method
	@Test
	public void testAdd() {
		Vector3d vec1 = new Vector3d(4, -2, 0.5);
		Vector3d vec2 = new Vector3d(5, -6, 7);
		Vector3d expected = new Vector3d(9, -8, 7.5);
		Vector3d vec3 = vec1.add(vec2);
		assertEquals(expected.getX(), vec3.getX());
		assertEquals(expected.getY(), vec3.getY());
		assertEquals(expected.getZ(), vec3.getZ());
		System.out.println("Testing Add method");
	}
}
