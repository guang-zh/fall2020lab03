/**
 * Guang Kun Zhang
 * 1942372
 * Lab3 Java III 
 */
package LinearAlgebra;

/**
 * @author guang
 * {@link https://gitlab.com/guang-zh/fall2020lab03.git}
 * @implNote Tests in the main method of this class
 */
public class Vector3d {
	
	/**
	 * 3 fields: x, y, z as doubles; class immutable
	 */
	private double x;
	private double y;
	private double z;

	/**
	 * Constructor takes 3 doubles as inputs and sets 3 fields
	 */
	public Vector3d(double x, double y, double z) {
		// sets 3 fields
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/**
	 * Get methods for all 3 fields
	 */
	public double getX() {
		return this.x;
	}
	public double getY() {
		return this.y;
	}
	public double getZ() {
		return this.z;
	}
	
	// Guang Kun Zhang 1942372
	/** 
	 * 4a. magnitude() method, use Math method for calculating
	 */
	public double magnitude() {
		return Math.sqrt(x*x + y*y + z*z);
	}
	
	/**
	 * 4b. dotProduct() method, return double from multiplying two vectors
	 */
	public double dotProduct(Vector3d vec2) {
		return (x*vec2.getX()+y*vec2.getY()+z*vec2.getZ());
	}
	
	/**
	 * 4c. add() method, create new Vector3d with this and input vectors
	 */
	public Vector3d add(Vector3d vec2) {
		double newX = x + vec2.getX();
		double newY = y + vec2.getY();
		double newZ = z + vec2.getZ();
		Vector3d vec3 = new Vector3d(newX, newY, newZ);
		return vec3;
	}

	/**
	 * @param args
	 * Testing the methods directly in this main method
	 */
	public static void main(String[] args) {
		// Simple Tests
		Vector3d vec1 = new Vector3d(3, 4, 5);
		System.out.println(vec1);
		System.out.println("x: "+ vec1.getX());
		System.out.println("y: "+ vec1.getY());
		System.out.println("z: "+ vec1.getZ());
		System.out.println("Magnitude: "+vec1.magnitude());
		Vector3d vec2 = new Vector3d(5, 2, 1);
		System.out.println("DotProduct: "+vec1.dotProduct(vec2));
		Vector3d vec3 = vec1.add(vec2);
		System.out.println("Vec3: "+vec3.getX()+" "+vec3.getY()+" "+vec3.getZ());

	}

}
